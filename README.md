# ASP.NET

#### 介绍
基于Sharp7连接西门子PLC



#### 使用说明

using Sharp7;


 static void Main(string[] args)
 {
    var client = new S7Client();
            int result = client.ConnectTo("127.0.0.1", 0, 1);
            if (result == 0)
            {
                Console.WriteLine("Connected to 127.0.0.1");
            }
            else
            {
                Console.WriteLine(client.ErrorText(result));
                Console.ReadKey();
                return;
            }

 }

#读取用法

            byte[] Buffer = new byte[18]; //用数组读取
            result = client.DBRead(1, 0, 18, Buffer);
            if (result != 0)
            {
                Console.WriteLine("Error: " + client.ErrorText(result));
            }
            int DB1DBW2 = S7.GetIntAt(Buffer, 2);//DB块里面的偏移量
            Console.WriteLine("DB1DBW2": " + DB1DBW2);

#写入用法
            db1Buffer = new byte[12];
            const int START_INDEX = 4;
            S7.SetRealAt(db1Buffer, 4 - START_INDEX, (float)54.36);
            S7.SetDIntAt(db1Buffer, 8 - START_INDEX, 555666);
            S7.SetDWordAt(db1Buffer, 12 - START_INDEX, 123456);
            result = client.DBWrite(1, START_INDEX, db1Buffer.Length, db1Buffer);
            if (result != 0)
            {
                Console.WriteLine("Error: " + client.ErrorText(result));
            }
    